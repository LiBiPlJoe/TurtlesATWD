
Controls
===
* When no monks are selected, arrow keys move the camera. 
* Space bar returns camera to center. 
* Click on a monk to select or deselect it. 
* While a monk is selected, arrow keys move the monk, and camera follows the monk.
* Only one monk can be selected at a time. 

Magic Stones
===
* Near a ritual spot
 * If not at the center of the ritual spot, and there are no other stones in the ritual spot, move toward the center of the ritual spot.
 * When a bad creature gets close enough, turn toward it and throw a peace sign at it.
 * After throwing a number of peace signs, disappear and respawn outside of the happy circle.
 * (not implemented) Throw particles at the two closest sitting monks.
* In the happy zone, outside of ritual spot
 * Follow the closest walking monk if close enough.
* Outside of the happy zone
 * Follow the closest walking monk if close enough.

Monks
===
* Sitting
 * (not implemented) Throw particles at the two closest stones if close enough.
 * If hit by bad creature, die.
 * Attract stones which are not already in their spot.
 * If clicked on, become the walking monk under control of the player.
* Walking
 * Controlled by player.
 * If hit by bad creature, die.
 * (not implemented) If close enough to two stones that are in their ritual spots, sit down.

Creatures
===
* Bad
 * Spawn outside of happy zone
 * (not implemented, are instead attracted to a moving spot near the center) Are attracted to the nearest monk
 * Become happy if hit by peace sign
* Happy
 * Wander around happy zone
 * (not implemented) When bumped into by bad creature, die

Happy Zone
===
* When all stones are in place
 * Expand slowly
 * If encompassing all of the unhappy zone, end level (win state)
* When not all stones are in place
 * (not implemented) Contract slowly
 * (not implemented) If radius zero, end level (fail state)

Etc
===
* If all monks are dead, end level (fail state)
* Camera: 
 * Default to center of ritual space
 * If not controlling a monk, arrows control camera
 * If controlling a monk, camera follows monk
