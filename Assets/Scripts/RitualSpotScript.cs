﻿using UnityEngine;
using System.Collections;

public class RitualSpotScript : MonoBehaviour {

    public bool hasStone;
    public GameObject myStone;
    Renderer myRend;

	// Use this for initialization
	void Awake () {
        hasStone = false;
        myStone = null;
        myRend = gameObject.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (hasStone)
        {
            myRend.enabled = false;
        }
        else
        {
            myRend.enabled = true;
            transform.Rotate(new Vector3(0, 0, 1f), 1f);
        }
	}
}
