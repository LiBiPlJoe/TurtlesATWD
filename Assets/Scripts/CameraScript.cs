﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

    public float speed = 5f;
    public Vector3 initPosition;
    GlobalScript globality;

	void Start () {
        initPosition = transform.position;
        globality = GameObject.Find("Globality").GetComponent<GlobalScript>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0f) * speed;
        if (globality.controlledMonk)
        {
            Vector3 newPosition = globality.controlledMonk.transform.position + movement * Time.deltaTime;
            // if (newPosition != transform.position) print("I'm at " + transform.position + " moving to " + newPosition);
            if (globality.InsideWorldRadius(newPosition))
            {
                globality.controlledMonk.transform.position = newPosition;
                transform.position = new Vector3(newPosition.x, newPosition.y, initPosition.z);
            }
        }
        else
        {
            Vector3 newPosition = transform.position + movement * Time.deltaTime;
            // if (newPosition != transform.position) print("I'm at " + transform.position + " moving to " + newPosition);
            if (globality.InsideWorldRadius(newPosition)) transform.position = newPosition;
        }
        if (Input.GetButtonDown("Jump"))
            transform.position = initPosition;

	}
}
