﻿using UnityEngine;
using System.Collections;

public class MonkScript : MonoBehaviour {

    [SerializeField]
    Sprite walking;
    [SerializeField]
    Sprite alive;
    [SerializeField]
    Sprite dead;
    public bool isAlive;
    public bool isWalking;

    SpriteRenderer myRend;

    GlobalScript globality;

	void Start () {
        globality = GameObject.Find("Globality").GetComponent<GlobalScript>();
        myRend = gameObject.GetComponent<SpriteRenderer>();
	    isAlive = true;
        isWalking = false;
	}
	
	void Update () {
        if (isWalking)
        {
            myRend.sprite = walking;
        }
        else if (isAlive) {
            myRend.sprite = alive;
        }
        else { myRend.sprite = dead; }
	}

    void OnMouseDown()
    {
        if (isAlive)
        {
            if (isWalking) { isWalking = false; globality.controlledMonk = null; }
            else if (!globality.controlledMonk) { isWalking = true; globality.controlledMonk = transform; }
            else print("Don't click me bro!");
        }
    }
}
