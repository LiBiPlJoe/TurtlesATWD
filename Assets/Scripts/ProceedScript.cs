﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ProceedScript : MonoBehaviour {

    public float timeToWait;
    float timeToProceed;

	void Awake () {
        timeToProceed = Time.time + timeToWait;
	}
	
	void Update () {
        print ("- " + timeToProceed + " - " + Time.time);
        if (Time.time > timeToProceed)
        {
            Proceed();
        } 	
	}

    void OnMouseDown()
    {
        Proceed();
    }

    void Proceed()
    {
        int currIndex = SceneManager.GetActiveScene().buildIndex;
        print("Finished scene " + currIndex);
        if (SceneManager.GetActiveScene().name == "YouWin")
            Application.Quit();
        if (SceneManager.sceneCount > currIndex)
            SceneManager.LoadScene(currIndex + 1);
        else
            Application.Quit();
    }
}
