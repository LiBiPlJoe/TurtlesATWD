﻿using UnityEngine;
using System.Collections;

public class StoneScript : MonoBehaviour {

    float nextShot;
    int shotsLeft;
	public bool allowShooting = true;
    public Rigidbody2D peace;
	public Rigidbody2D stone;
    public Rigidbody2D lastShot;
    public bool atSpot;
    public GameObject mySpot;
    public bool hasDestination;
    public Vector3 destination;
    public float arriveAtDest;
    ParticleSystem myParticles;

	// Use this for initialization
	void Awake () {
        nextShot = Time.time + 1f;
        shotsLeft = 4;
		allowShooting = true;
        mySpot = null;
        atSpot = false;
        hasDestination = false;
        destination = Vector3.zero;
        arriveAtDest = Time.time;
        myParticles = gameObject.GetComponent<ParticleSystem>();
        myParticles.enableEmission = false;
	}
	
    GlobalScript globality;

    void Start()
    {
        globality = GameObject.Find("Globality").GetComponent<GlobalScript>();
    }

    void Update()
    {
        if (hasDestination)
        {
            transform.position = Vector3.Lerp(transform.position, destination, (Time.time - (arriveAtDest - 1f)));
            if (transform.position == destination)
            {
                hasDestination = false;
                if (mySpot)
                {
                    atSpot = true;
                    allowShooting = true;
                    myParticles.enableEmission = true;
                }
            }
        }
    }

	void FixedUpdate () {
        if (atSpot)
        {
            if (shotsLeft <= 0)
            {
                RitualSpotScript temp = mySpot.GetComponent<RitualSpotScript>();
                temp.hasStone = false;
                ResetOutside();
            }
            if (Time.time > nextShot)
            {
                if (LookAtEvil())
                {
                    print("I saw evil!");
                    ShootPeace();
                    nextShot = Time.time + 1f;
                }
            }
        }
        else
        {
            if (globality.InsideHippyRadius(transform.position) && mySpot == null)
            {
                GameObject[] ritualSpots = GameObject.FindGameObjectsWithTag("spot");
                foreach (GameObject ritualSpot in ritualSpots)
                {
                    if (Vector3.Distance(transform.position, ritualSpot.transform.position) < 5f)
                    {
                        RitualSpotScript temp = ritualSpot.GetComponent<RitualSpotScript>();
                        if (!temp.hasStone)
                        {
                            temp.hasStone = true;
                            temp.myStone = gameObject;
                            mySpot = ritualSpot;
                            destination = ritualSpot.transform.position;
                            arriveAtDest = Time.time + 1f;
                            hasDestination = true;
                            break;
                        }
                    }
                }
            }
            if (mySpot == null && !hasDestination)
            {
                GameObject[] monks = GameObject.FindGameObjectsWithTag("monk");
                foreach (GameObject monk in monks)
                {
                    if (Vector3.Distance(transform.position, monk.transform.position) < 3f)
                    {
                        MonkScript temp = monk.GetComponent<MonkScript>();
                        if (temp.isWalking)
                        {
                            destination = (transform.position + monk.transform.position) / 2f;
                            arriveAtDest = Time.time + 1f;
                            hasDestination = true;
                            break;
                        }
                    }
                }
            }
        }
	}

    void ResetOutside()
    {
        myParticles.enableEmission = false;
        Vector3 newPosition = new Vector3(0f, (globality.hippyRadius + globality.worldRadius) / 2f, transform.position.z);
        transform.position = newPosition;
        nextShot = Time.time + 1f;
        shotsLeft = 10;
        allowShooting = false;
        mySpot = null;
        atSpot = false;
        hasDestination = false;
        destination = Vector3.zero;
        arriveAtDest = Time.time;
    }

    bool LookAtEvil()
    {
        GameObject evilThing = GameObject.FindGameObjectWithTag("evil");
        if (!evilThing) return false;
        // print("Evil seen at " + evilThing.transform.position);
        // Thank you http://answers.unity3d.com/questions/585035/lookat-2d-equivalent-.html
        Vector3 diff = evilThing.transform.position - transform.position;
        //print("evil thing at " + evilThing.transform.position + ", I'm at " + transform.position);
        //print("  that makes a diff of " + diff + ", with a magnitude of " + diff.magnitude);
        if (diff.magnitude < 3f)
        {
            diff.Normalize();
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
            return true;
        }
        else { return false; }
    }
		
	void CreateNewStones() {

		int chance = Mathf.FloorToInt(Mathf.Floor(Random.value * 100)) % 3;

		for (int i = 0; i < chance; i++) {
			GameObject newStone = Object.Instantiate (gameObject, new Vector3(Random.Range(transform.position.x - 5f,transform.position.x + 5f), Random.Range(transform.position.y - 5f,transform.position.y + 5f), transform.position.z), Quaternion.identity) as GameObject;
			StoneScript gScript = newStone.GetComponent<StoneScript> ();
			if (gScript != null) {
				gScript.allowShooting = false;
				print (this.name + " created a new stone!!!");
			}
		}
	}

    void ShootPeace()
    {
		if (allowShooting == false) {
			print ("Hey Don't shoot me!!!!");
			return;
		}

        Rigidbody2D temp = Instantiate(peace, transform.position, transform.rotation) as Rigidbody2D;
        temp.AddForce(new Vector2(transform.up.x, transform.up.y) * 5f, ForceMode2D.Impulse);
        if (lastShot) GameObject.Destroy(lastShot.gameObject);
        lastShot = temp;
        shotsLeft -= 1;
    }
}
