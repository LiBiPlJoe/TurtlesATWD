﻿using UnityEngine;
using System.Collections;

public class EvilScript : MonoBehaviour {

    public Vector3 destination;
    [SerializeField]
    Sprite evil;
    [SerializeField]
    Sprite good;
    [SerializeField]
    Sprite dead;
    public bool isEvil;
    public bool isDead;
    public bool hasDestination;

    SpriteRenderer myRend;

    GlobalScript globality;

    void Awake()
    {
        isEvil = true;
        isDead = false;
        hasDestination = false;
    }

    void Start()
    {
        globality = GameObject.Find("Globality").GetComponent<GlobalScript>();
        myRend = gameObject.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (isEvil)
        {
            myRend.sprite = evil;
        }
        else if (isDead)
        {
            myRend.sprite = dead;
        }
        else { myRend.sprite = good; }
    }
	
    void FixedUpdate () {
        if (isEvil)
        {
            destination = new Vector3(Mathf.Sin(Time.time), Mathf.Sin(Time.time * 2f), transform.position.z) * 5f;
            destination.z = transform.position.z;
        }
        else if (!hasDestination)
        {
            destination = new Vector3(Random.Range(-1 * globality.hippyRadius, globality.hippyRadius),
                Random.Range(-1 * globality.hippyRadius, globality.hippyRadius), transform.position.z);
            while (!globality.InsideHippyRadius(destination)) {
                destination = new Vector3(Random.Range(-1 * globality.hippyRadius, globality.hippyRadius),
                    Random.Range(-1 * globality.hippyRadius, globality.hippyRadius), transform.position.z);
            }
            hasDestination = true;
        }
        Vector3 diff = destination - transform.position;
        if (diff.magnitude < 0.1f) hasDestination = false;
        diff.Normalize();
        if (!isEvil) diff *= 0.1f;
        else if (globality.InsideHippyRadius(transform.position)) diff *= 0.5f;
        transform.position = transform.position + (0.1f * diff);
	}

    void OnTriggerEnter2D(Collider2D target)
    {
        // print("touched " + target.tag);
        if (target.tag == "peace")
        {
            // transform.position = new Vector3(5f, 5f, transform.position.z);
            isEvil = false;
            tag = "good";
        }
        if (isEvil && target.tag == "monk")
        {
            // print("Touched monk");
            MonkScript temp = target.GetComponent<MonkScript>();
            if (temp.isAlive)
            {
                if (globality.controlledMonk && globality.controlledMonk.transform == target.transform) globality.controlledMonk = null;
                temp.isWalking = false;
                temp.isAlive = false;
                GameObject.Destroy(gameObject);
            }
        }
    }
}
