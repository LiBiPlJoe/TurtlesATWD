﻿using UnityEngine;
using System.Collections;

public class HappyRingScript : MonoBehaviour {

    GlobalScript globality;
    SpriteRenderer myRend;

    void Start()
    {
        globality = GameObject.Find("Globality").GetComponent<GlobalScript>();
        myRend = gameObject.GetComponent<SpriteRenderer>();
    }
	
	void Update () {
        if (globality.inactiveStones)
        {
            myRend.enabled = false;
        }
        else
        {
            myRend.enabled = true;
            transform.Rotate(new Vector3(0, 0, 1f), Time.deltaTime * 50f);
        }
	}
}
