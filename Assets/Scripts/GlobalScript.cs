﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GlobalScript : MonoBehaviour {

    public float hippyRadius, worldRadius;
    public float creatureRate, timeForCreature;
    public int totalStones;
    public bool inactiveStones;

    [SerializeField]
    Transform creature;

    public Transform controlledMonk;

	void Awake () {
        timeForCreature = Time.time + creatureRate;
        inactiveStones = true;
	}

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            print("I quit!");
            Application.Quit();
        }
    }

	void FixedUpdate () {
        bool livingMonks = false;
        GameObject[] monks = GameObject.FindGameObjectsWithTag("monk");
        foreach (GameObject monk in monks) {
            if (monk.GetComponent<MonkScript>().isAlive)
            {
                livingMonks = true;
                break;
            }
        } 
        if (!livingMonks)
        {
            SceneManager.LoadScene("GameOver");
        }
        inactiveStones = false;
        GameObject[] stones = GameObject.FindGameObjectsWithTag("magic");
        foreach (GameObject stone in stones)
        {
            if (stone.GetComponent<StoneScript>().atSpot == false)
            {
                inactiveStones = true;
                break;
            }
        }
        if (!inactiveStones)
        {
            float expandRate = 0.001f / totalStones;
            hippyRadius *= (1f + expandRate);
            if (hippyRadius > worldRadius)
            {
                int currIndex = SceneManager.GetActiveScene().buildIndex;
                print("Finished scene " + currIndex);
                SceneManager.LoadScene(currIndex + 1);
            }
        }
        if (Time.time > timeForCreature)
        {
            if (SpawnNewCreature()) timeForCreature = Time.time + creatureRate;
        }
	}

    public bool InsideHippyRadius(Vector2 pos)
    {
        if (pos.magnitude < hippyRadius) return true;
        else return false;
    }
    public bool InsideHippyRadius(Vector3 pos)
    {
        pos.z = 0f;
        // print("IHR position " + pos + ", magnitude " + pos.magnitude + ", hippyRadius " + hippyRadius);
        if (pos.magnitude < hippyRadius) return true;
        else return false;
    }
    public bool InsideWorldRadius(Vector2 pos)
    {
        if (pos.magnitude < worldRadius) return true;
        else return false;
    }
    public bool InsideWorldRadius(Vector3 pos)
    {
        pos.z = 0f;
        // print("Magnitude is " + pos.magnitude);
        if (pos.magnitude < worldRadius) return true;
        else return false;
    }

    bool SpawnNewCreature()
    {

        Vector3 newPosition = new Vector3(Random.Range(worldRadius * -1, worldRadius),
                                          Random.Range(worldRadius * -1, worldRadius), 0f);
        if (InsideWorldRadius(newPosition) && !InsideHippyRadius(newPosition))
        {
            Transform temp = Instantiate(creature, newPosition, Quaternion.identity) as Transform;
            if (temp) return true;
        }
        return false;
    }

}
