﻿using UnityEngine;
using System.Collections;

public class BackgroundScript : MonoBehaviour {

    GlobalScript globality;

	void Start () {
        globality = GameObject.Find("Globality").GetComponent<GlobalScript>();
        GameObject[] magics = GameObject.FindGameObjectsWithTag("magic");
        globality.hippyRadius = magics.Length * 2f;
    }
	
	// Update is called once per frame
	void Update () {
        transform.localScale = new Vector3(2f, 2f, 0.1f) * globality.hippyRadius;
	}
}
